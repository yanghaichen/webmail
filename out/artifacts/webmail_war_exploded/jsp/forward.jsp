<%@ page import="com.hstc.pojo.Users" %>
<%@ page import="com.hstc.common.Const" %>
<%@ page import="javax.mail.*" %>
<%@ page import="com.hstc.util.PraseMimeMsg" %>
<%@ page import="javax.mail.internet.MimeMessage" %>
<%@ page import="javax.mail.Part" %>
<%@ page import="com.hstc.dto.AttachmentDTO" %>
<%@ page import="java.util.List" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>转发页面</title>
    <link rel="stylesheet" href="<%=basePath%>assets/css/amazeui.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<%=basePath%>assets/css/core.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/menu.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/index.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/admin.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/typography.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/form.css" />
    <link rel="stylesheet"  href="<%=basePath %>assets/kindeditor/themes/default/default.css" />
</head>
<body>
<!-- Begin page -->
<header class="am-topbar am-topbar-fixed-top">
    <div class="am-topbar-left am-hide-sm-only">
        <a href="#" class="logo">
            <c:choose>
            <c:when test="${not empty sessionScope.currentUser}"><span><c:out  value="${sessionScope.currentUser.username}" /></c:when>
                <c:otherwise>
                    未登陆
                </c:otherwise>
            </c:choose></span><i class="zmdi zmdi-layers"></i></a>&nbsp;&nbsp;<%--<a  href="<%=basePath%>user/logout">退出</a>--%>
    </div>
    <input type="hidden" class="msg" value="${msg}"/>
    <div class="contain">
        <ul class="am-nav am-navbar-nav am-navbar-left">

            <li><h4 class="page-title">邮件转发</h4></li>
        </ul>


    </div>
</header>
<!-- end page -->


<div class="admin">
    <!--<div class="am-g">-->
    <!-- ========== Left Sidebar Start ========== -->
    <!--<div class="left side-menu am-hide-sm-only am-u-md-1 am-padding-0">
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 548px;">
            <div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 548px;">-->
    <!-- sidebar start -->
    <div class="admin-sidebar am-offcanvas  am-padding-0" id="admin-offcanvas">
        <div class="am-offcanvas-bar admin-offcanvas-bar">
            <!-- User -->
            <div class="user-box am-hide-sm-only">
                <div class="user-img">
                    <img src="../assets/img/avatar-1.jpg" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status offline"><i class="am-icon-dot-circle-o" aria-hidden="true"></i></div>
                </div>
                <h5><a href="#">Mat Helme</a> </h5>
                <ul class="list-inline">
                    <li>
                        <a href="#">
                            <i class="am-icon-cog" aria-hidden="true"></i>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="text-custom">
                            <i class="am-icon-cog" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- End User -->

            <ul class="am-list admin-sidebar-list">
                <li><a href="<%=basePath%>jsp/index.jsp"><span class="am-icon-home"></span> 首页</a></li>
                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav1'}"><span class="am-icon-table"></span> 邮件 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                    <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav1">

                        <li><a href="<%=basePath%>jsp/index.jsp" class="am-cf"> <span>收件箱</span></a></li>
                        <li><a href="<%=basePath%>jsp/sendEmail.jsp">写邮件</a></li>
                        <li><a href="<%=basePath%>jsp/index1.jsp">已发送</a></li>
                        <li><a href="<%=basePath%>jsp/draft.jsp">草稿箱</a></li>
                        <li><a href="<%=basePath%>jsp/delete.jsp">已删除</a></li>
                    </ul>
                </li>
                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav2'}"><span class="am-icon-file"></span> 我的好友 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                    <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav2">
                        <c:choose>
                            <c:when test="${not empty sessionScope.userNameList}">
                                <c:forEach var="user" items="${sessionScope.userNameList}">
                                    <li><a href="javascript:void(0);" class="am-cf  communion"> <span>${user.username}</span></a></li>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </li>

                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav3'}">  </a>
                    <ul class="am-list am-collapse admin-sidebar-sub " id="collapse-nav3">
                        <li><a href="javascript:void(0);" class="am-cf"> <span></span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- sidebar end -->

    <!--</div>
</div>
</div>-->
    <!-- ========== Left Sidebar end ========== -->

    <%
        Users user = (Users) request.getSession().getAttribute(Const.USERNAME);
        // 2、读取邮件夹
        Folder folder = (Folder)request.getSession().getAttribute("folder");
        // 获取邮件夹中第i封邮件信息
        Integer msgnum = Integer.parseInt(request.getParameter("msgnum"));
        String subject = null;
        String from = null;
        String to = null;
        String content = null;
        String sendDate = null;
        List<AttachmentDTO> attachmentDTOList = null;
        try {
            if(!folder.isOpen()){
                folder.open(Folder.READ_WRITE);
            }
            Message[] messages = folder.getMessages();
            Message message =   messages[msgnum];
            PraseMimeMsg pmm = new PraseMimeMsg((MimeMessage) message,session);
            subject = pmm.getSubject();
            //获取回复地址
            to = pmm.getMailAddress("to");
            from = pmm.getFrom();
            pmm.getMailContent((Part)message);
            content = pmm.getBodyText();
            sendDate = pmm.getSentDate();
            attachmentDTOList = pmm.handleMultipart();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch(Exception e1) {
            e1.printStackTrace();
        }
    %>

    <!--	<div class="am-g">-->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="am-g">
                <!-- Row start -->
                <div class="am-u-sm-12">
                    <div class="card-box">
                        <form action="<%=basePath%>mail/fowardmail" enctype="multipart/form-data"  method="post" class="am-form" data-am-validator>
                            <fieldset>
                                <legend>邮件转发</legend>
                                <div class="am-form-group">
                                    <label for="doc-vld-name-2">收件人：</label>
                                    <input type="text" name="toList" id="doc-vld-name-2" value="" minlength="3"  />
                                </div>
                                <input name="msgnum" type="hidden" value="<%=msgnum%>">
                                <div class="am-form-group">
                                    <label for="doc-vld-email-2">标题：</label>
                                    <input type="text" name="subject" id="doc-vld-email-2" value="转发:<%=subject%>"/>
                                </div>

                                <div class="am-form-group">
                                    <label for="doc-vld-url-2">内容：</label>
                                    <textarea  name="content" id="doc-vld-url-2" style="width:800px;height: 400px;">

                                        <div><br></div>
                                        <div><br></div>
                                        <div style="font-size: 12px;font-family: Arial Narrow;padding:2px 0 2px 0;">------------------&nbsp;原始邮件&nbsp;------------------</div>
                                        <div style="font-size: 12px;background:#efefef;padding:8px;">
                                            <div><b>发件人:</b>&nbsp;<%=from%></div>
                                            <div><b>发送时间:</b>&nbsp;<%=sendDate%></div>
                                            <div><b>收件人:</b>&nbsp;<%=to%><wbr></div>
                                            <div></div>
                                            <div><b>主题:</b>&nbsp;<%=subject%></div>
                                        </div>
                                        <div><br></div>
                                               <%--<p>---------原邮件内容 ---------</p>
                                               <p>主题：<%=subject%><p/>
                                               <p>发送人：<%=from%><p/>
                                               <p>收件人：<%=to%><p/>
                                               <p>发送时间：<%=sendDate%><p/>--%>
                                               <p><%=content%><p/>
								      </textarea>

                                </div>
                                <div class="am-form-group">
                                    <label for="doc-vld-url-5">附件：</label>
                                    <input type="file" name="attFile" id="attFile" id="doc-vld-url-5" multiple="multiple"/>
                                </div>
                                <%--<%
                                    for(int i  = 0; i < attachmentDTOList.size(); i++) {
                                %>
                                附件：<a href="<%=basePath%>HandleAttachServlet?msgnum=<%=msgnum%>&&bodynum=<%=((AttachmentDTO)attachmentDTOList.get(i)).getMpid()%>&&filename=<%=((AttachmentDTO)attachmentDTOList.get(i)).getAttName()%>"> <%=((AttachmentDTO)attachmentDTOList.get(i)).getAttName()%></a><br/>
                                <%
                                    }
                                %>--%>
                                <hr/>
                                <input  class="am-btn am-btn-secondary"  type="submit" value="转发"/>
                                <input class="am-btn am-btn-secondary back" type="button" value="取消" />
                            </fieldset>
                        </form>


                    </div>
                </div>
                <!-- Row end -->
            </div>

        </div>
    </div>
    <!-- end right Content here -->
    <!--</div>-->
</div>
</div>

<!-- navbar -->
<a href="admin-offcanvas" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"><!--<i class="fa fa-bars" aria-hidden="true"></i>--></a>

<script type="text/javascript" src="<%=basePath%>assets/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=basePath%>assets/js/amazeui.min.js"></script>
<script src="<%=basePath %>assets/kindeditor/kindeditor-all-min.js"></script>
<script src="<%=basePath %>assets/kindeditor/lang/zh-CN.js"></script>
</body>
<script>
    KindEditor.ready(function(K) {
        //声明两个数组
        var uploadimgsrc = [];  //所有上传的图片
        var saveimgsrc = [];	//最后保存下来的图片
        //"image","multiimage",
        editor = K.create('textarea[name="content"]', {
            items:["undo","redo","cut","copy","paste","plainpaste",
                "justifyleft","justifycenter","justifyright","justifyfull","indent",
                "outdent","formatblock","fontname","fontsize","forecolor",
                "hilitecolor","bold","italic","underline","image","fullscreen"],
            height:'320px',width:'850px',resizeMode:0,autoOnsubmitMode:'true',
            uploadJson:'<%=basePath %>mail/uploadImage',
            imageSizeLimit : '5MB', 			//批量上传图片单张最大容量
            imageUploadLimit : 20, 			//批量上传图片同时上传最多个数
            filePostName:"uploadFile",		//上传组件名
            allowImageRemote: false,         //去掉网络图片的样式
            allowImageUpload: true, 			//多图上传
            afterBlur: function(){  			//编辑器失去焦点(blur)时执行的回调函数（将编辑器的HTML数据同步到textarea）
                this.sync();
                saveimgsrc = pickimg(editor.html());
                //合并数组
                var allimgsrc = uploadimgsrc.concat(saveimgsrc);
                //删除相同的元素
                var uniq = new arr_uniq(allimgsrc);
                uniq.uniqs();
                resultimg = slectArray(allimgsrc,saveimgsrc);

            },
            afterUpload : function(data) { 			//上	传文件后执行的回调函数，必须为3个参数error,msg,url
                this.sync();
                uploadimgsrc.push(data.split("upload/contentimg/")[1]);
            },
            afterCreate:function() {			       //保存新上传的图片路径，设置编辑器创建后执行的回调函数
                this.sync();

            },

        });

    });


    $('.back').click(function(){
        window.location.href = "<%=basePath%>jsp/index.jsp";
    });
    //给好友发邮件
    $('.communion').click(function(){
        var username = $(this).text();
        username = username+'@zjnszn.xyz<'+ username +'@zjnszn.xyz>';
        window.location.href = "<%=basePath%>jsp/sendEmail.jsp?toList=" + username;
    });
</script>
</html>
