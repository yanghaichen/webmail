<%@ page import="com.hstc.common.ServerResponse" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="<%=basePath%>assets/css/core.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/menu.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/amazeui.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/component.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/form.css" />
</head>
<body>
<div class="account-pages">
    <div class="wrapper-page">
        <div class="text-center">
            <a href="index.html" class="logo"><span>邮箱<span>系统</span></span></a>
        </div>
        <%
            ServerResponse  serverResponse = (ServerResponse)request.getAttribute("serverResponse");
            String msg = null;
            if(serverResponse != null){
                msg = serverResponse.getMsg();
            }
        %>
        <div class="m-t-40 card-box">
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">注册</h4>
            </div>
            <div class="panel-body">
                <form class="am-form" id="myform" action="<%=basePath%>user/register" method="post">
                    <div class="am-g">
                        <div class="am-form-group">
                            <input type="text" name="username" class="am-radius"  placeholder="新用户名">
                        </div>

                        <div class="am-form-group form-horizontal m-t-20">
                            <input type="password" name="pwd" class="am-radius"  placeholder="密码">
                        </div>

                        <div class="am-form-group form-horizontal m-t-20">
                            <input type="password" name="repeatpwd" class="am-radius"  placeholder="再次输入密码">
                        </div>


                        <div class="am-form-group ">
                            <input type="submit" class="am-btn am-btn-primary am-radius register_btn" value="确定" style="width: 100%;height: 100%;"><br/><br/>
                            <a href="<%=basePath%>jsp/login.jsp" class="am-btn am-btn-primary am-radius" style="width: 100%;height: 100%;">返回</a>
                            </div>


                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
</body>
<script src="<%=basePath%>assets/js/jquery-2.1.0.js"></script>
<script src="<%=basePath%>assets/js/amazeui.min.js"></script>

<script>
    $(".register_btn").click(function(){

        var form = new FormData(document.getElementById("myform"));
        var username = $("input[name='username']").val();
        var pwd = $("input[name='pwd']").val();
        var repeatpwd = $("input[name='repeatpwd']").val();
        if(username.trim() == "" || username == null) {
            alert("用户名不能为空");
            return false;
        }
        if(pwd.trim() == "" || pwd == null) {
            alert("密码不能为空");
            return false;
        }else if(pwd != repeatpwd){
            alert("密码输入不一致");
            return false;
        }
    });

    $(function(){
        var msg = '<%=msg%>';

        if(msg.trim() != "" && msg != 'null'){
            alert(msg);
        }
    });


</script>

</html>
