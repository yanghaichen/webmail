package com.hstc.dao;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.hstc.db.DbFactory;
import com.hstc.pojo.Users;

/**
 * @auther 倪万佳
 * @date 2018/11/6 9:57
 */
public class UserDAO {


    //查找用户
    public boolean findUser(Users users, Connection conn)
    {

        String sql = "select count(*) from users where username = ?" ;
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try
        {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1,users.getUsername());
            rs = preparedStatement.executeQuery();

            rs.next();
            if(rs.getInt(1) != 0) {
                DbFactory.closeAll(rs,preparedStatement, conn);
                return true;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            DbFactory.closeAll(rs,preparedStatement, conn);
        }
        return false;
    }

    //用户登录
    public boolean login(Users user, Connection connection) {
        String sql = "select count(*) from users where username = ? and pwdHash = ?";
        PreparedStatement preparedStatement = null;
        ResultSet rs=null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user.getUsername());
            preparedStatement.setString(2, user.getPwdHash());
             rs = preparedStatement.executeQuery();
            //TODO后期可以添加用户详情资料
            rs.next();
            if(rs.getInt(1) != 0) {
                return true;
            }else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection=DbFactory.getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, user.getUsername());
                preparedStatement.setString(2, user.getPwdHash());
                rs = preparedStatement.executeQuery();
                //TODO后期可以添加用户详情资料
                rs.next();
                if (rs.getInt(1) != 0) {
                    return true;
                } else {
                    return false;
                }
            }catch (SQLException e1){
                e1.printStackTrace();
            }
        } finally
        {
            DbFactory.closeAll(rs,preparedStatement, connection);
        }
         return false;
    }

    //添加用戶
    public boolean addUser(Users user, Connection connection) {
        String sql = "insert into users(username,pwdHash,pwdAlgorithm,useForwarding,useAlias) values(?,?,?,?,?)";
        int num = -1;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user.getUsername());
            preparedStatement.setString(2,user.getPwdHash());
            preparedStatement.setString(3,user.getPwdAlgorithm());
            preparedStatement.setInt(4,user.getUseForwarding());
            preparedStatement.setInt(5, user.getUseAlias());
            num = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally
        {
            DbFactory.closeAll(null,preparedStatement, connection);
        }

        if(num != -1) {
            return true;
        }else {
            return false;
        }
    }

    //获取用户列表
    public List<Users> getUserList(Users users, Connection conn)
    {

        String sql = "select username from users where username != ?" ;
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        List<Users> list = new ArrayList<Users>();
        try
        {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1,users.getUsername());
            rs = preparedStatement.executeQuery();
            String username = null;
            Users user = null;
            while(rs.next()){
                username = rs.getString(1);
                user = new Users();
                user.setUsername(username);
                list.add(user);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            DbFactory.closeAll(rs,preparedStatement, conn);
        }
        return list;
    }


}
