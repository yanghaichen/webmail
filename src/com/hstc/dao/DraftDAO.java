package com.hstc.dao;

import com.hstc.db.DbFactory;
import com.hstc.pojo.Draft;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @auther 倪万佳
 * @date 2018/11/12 7:48
 */
public class DraftDAO {

    //添加草稿
    public boolean addUser(Draft draft, Connection connection) {
        String sql = "insert into draft(file_name,username,recipients,title) values(?,?,?,?)";
        int num = -1;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,draft.getFileName());
            preparedStatement.setString(2,draft.getUsername());
            preparedStatement.setString(3,draft.getRecipients());
            preparedStatement.setString(4,draft.getTitle());
            num = preparedStatement.executeUpdate();
            //获取自增组件
//            ResultSet rs = preparedStatement.getGeneratedKeys();
//            if (rs.next()) {
//                draft.setId(rs.getInt(1));
//            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally
        {
            DbFactory.closeAll(null,preparedStatement, connection);
        }

        if(num != -1) {
            return true;
        }else {
            return false;
        }
    }

    //获取草稿列表
    public List<Draft> getDraftList(Draft draft, Connection conn)
    {
        String sql = "select id, file_name,recipients,title from draft where username = ?" ;
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        List<Draft> list = new ArrayList<Draft>();
        try
        {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1,draft.getUsername());
            rs = preparedStatement.executeQuery();
            String fileName = null;
            Integer id = null;
            String recipients=null;
            String title=null;
            Draft draft1 = null;
            while(rs.next()){
                id = rs.getInt(1);
                fileName = rs.getString(2);
                recipients = rs.getString(3);
                title = rs.getString(4);
                draft = new Draft(id, fileName,recipients,title);
                list.add(draft);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            DbFactory.closeAll(rs,preparedStatement, conn);
        }
        return list;
    }

    //删除草稿纸
    public Boolean killDraft(Integer id,Connection connection) {
        String sql = "delete from draft where id = ?";
        int num = -1;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            num = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally
        {
            DbFactory.closeAll(null,preparedStatement, connection);
        }
        if(num != -1) {
            return true;
        }else {
            return false;
        }
    }

    //获取文件名
    public String getFileName(Integer id, Connection connection) {
        String sql = "select file_name from draft where id = ?" ;
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        String fileName = null;
        try
        {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            rs = preparedStatement.executeQuery();

            if(rs.next()){
                fileName = rs.getString(1);

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            DbFactory.closeAll(rs,preparedStatement, connection);
        }
        return  fileName;
    }
}
