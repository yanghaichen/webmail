package com.hstc.pojo;

/**
 * @auther 倪万佳
 * @date 2018/11/12 12:16
 * 邮箱状态
 */
public class Status {

    private Integer id;
    private String username;
    private String uid;
    private Integer seen;
    public Status() {
    }

    public Status(String uid, Integer seen) {
        this.uid = uid;
        this.seen = seen;
    }

    public Status(String username, String uid) {
        this.username = username;
        this.uid = uid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getSeen() {
        return seen;
    }

    public void setSeen(Integer seen) {
        this.seen = seen;
    }
}
