package com.hstc.pojo;

/**
 * @auther 倪万佳
 * @date 2018/11/6 10:40
 * 邮箱用户表
 */
public class Users {

    private String username;
    private String pwdHash;
    private String pwdAlgorithm;
    private Integer  useForwarding;
    private String forwardDestination;
    private Integer useAlias;
    private String alias;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwdHash() {
        return pwdHash;
    }

    public void setPwdHash(String pwdHash) {
        this.pwdHash = pwdHash;
    }

    public String getPwdAlgorithm() {
        return pwdAlgorithm;
    }

    public void setPwdAlgorithm(String pwdAlgorithm) {
        this.pwdAlgorithm = pwdAlgorithm;
    }

    public Integer getUseForwarding() {
        return useForwarding;
    }

    public void setUseForwarding(Integer useForwarding) {
        this.useForwarding = useForwarding;
    }

    public String getForwardDestination() {
        return forwardDestination;
    }

    public void setForwardDestination(String forwardDestination) {
        this.forwardDestination = forwardDestination;
    }

    public Integer getUseAlias() {
        return useAlias;
    }

    public void setUseAlias(Integer useAlias) {
        this.useAlias = useAlias;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
