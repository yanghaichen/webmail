package com.hstc.pojo;

/**
 * @auther 倪万佳
 * @date 2018/11/12 7:51
 */
public class Draft {

    Integer id;
    String fileName;
    String username;
    String recipients;
    String title;

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Draft(String fileName, String recipients, String title) {
        this.fileName = fileName;
        this.recipients = recipients;
        this.title = title;
    }

    public Draft(String fileName) {
        this.fileName = fileName;
    }

    public Draft() {
    }

    public Draft(Integer id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    public Draft(Integer id, String fileName, String recipients, String title) {
        this.id = id;
        this.fileName = fileName;
        this.recipients = recipients;
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
