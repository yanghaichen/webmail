package com.hstc.servlet;

import com.hstc.common.Const;
import com.hstc.common.ServerResponse;
import com.hstc.dao.UserDAO;
import com.hstc.db.DbFactory;
import com.hstc.pojo.Users;
import com.hstc.util.DigestUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;


/**
 * 用户处理相关的Servlet
 */
@WebServlet(name = "UserServlet")
public class UserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            String url = request.getRequestURI();
            String basePath = "/jsp/";
            if(url.toUpperCase().indexOf("LOGIN") != -1){
                /*Cookie cookie = new Cookie("rltoken", Math.random()+"");
                cookie.setPath("/");
                //cookie.setMaxAge(0);
                response.addCookie(cookie);
                request.getSession().invalidate();*/
                Users user = new Users();
                user.setUsername(request.getParameter("username"));
                String pwd = request.getParameter("pwd");
                try {
                    user.setPwdHash(DigestUtil.digestString(pwd, "SHA"));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                Connection connection = DbFactory.getConnection();
                UserDAO userDAO = new UserDAO();
                if(userDAO.login(user, connection)){
                    user.setPwdHash(pwd);
                    request.getSession().setAttribute(Const.USERNAME, user);

                    //获取好友列表
                    List<Users> list = userDAO.getUserList(user, DbFactory.getConnection());
                    request.getSession().setAttribute(Const.USERNAMELIST, list);
                    url = basePath + "index.jsp";
                }else {
                    request.setAttribute("serverResponse", ServerResponse.createByErrorMessage("用户名或者密码错误"));
                    url = basePath + "login.jsp";
                }
            }else if(url.toUpperCase().indexOf("LOGOUT") != -1){
                request.getSession().invalidate();
                url = basePath +  "login.jsp";
            }else if(url.toUpperCase().indexOf("REGISTER") != -1) {
                Connection connection = DbFactory.getConnection();
                UserDAO userDAO = new UserDAO();
                Users user = new Users();
                user.setUsername(request.getParameter("username"));
                if(userDAO.findUser(user, connection)) {
                    url = basePath + "register.jsp";
                    request.setAttribute("serverResponse", ServerResponse.createByErrorMessage("用户名已存在，请重新填写"));
                }else {

                    try {
                        user.setPwdHash(DigestUtil.digestString(request.getParameter("pwd"), "SHA"));
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    user.setPwdAlgorithm("SHA");
                    user.setUseForwarding(0);
                    user.setUseAlias(0);
                    if(userDAO.addUser(user, DbFactory.getConnection())){
                        request.setAttribute("serverResponse", ServerResponse.createByErrorMessage("注册成功"));
                        url = basePath + "login.jsp";
                    }else{
                        request.setAttribute("serverResponse", ServerResponse.createByErrorMessage("注册失败"));
                        url = basePath +  "register.jsp";
                    }
                }
            }
      request.getRequestDispatcher(url).forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          doPost(request, response);
    }
}
