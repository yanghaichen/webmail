package com.hstc.common;

/**
 * @auther 倪万佳
 * @date 2018/11/6 8:13
 */
public class Const {

    /*//邮箱服务器的ip地址
    public static final String SERVER = "192.168.43.54";
    //已登陆的用户信息
    public static final String USERNAME = "currentUser";
    //用户列表
    public static final String USERNAMELIST = "userNameList";
    //文件存放地址
    public static final String FILESAVEPATH = "D:/webmaildir/";*/
    //邮箱服务器的ip地址
    public static final String SERVER = "119.3.101.234";
    //已登陆的用户信息
    public static final String USERNAME = "currentUser";
    //用户列表
    public static final String USERNAMELIST = "userNameList";
    //文件存放地址
    public static final String FILESAVEPATH = "/root/webmaildir/";
    //public static final String FILESAVEPATH = "D:/webmaildir/";
    public static final String IMAGE = "/assets/kindeditor/uploadImage/";

}
