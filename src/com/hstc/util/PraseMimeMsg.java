package com.hstc.util;

import com.hstc.common.Const;
import com.hstc.dao.StatusDAO;
import com.hstc.db.DbFactory;
import com.hstc.dto.AttachmentDTO;
import com.hstc.pojo.Status;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.*;

/**
 * @auther 倪万佳
 * @date 2018/11/9 23:33
 * 解析邮件的类
 */
public class PraseMimeMsg {

    private MimeMessage mimeMessage = null;
    private StringBuffer bodytext = new StringBuffer();
    private Map<String,String> map=new HashMap<>();
    private HttpSession session;
    public PraseMimeMsg(MimeMessage mimeMessage,HttpSession session)
    {
        this.mimeMessage = mimeMessage;
        this.session=session;
    }

    public void setMimeMessage(MimeMessage mimeMessage)
    {
        this.mimeMessage = mimeMessage;
    }

    // 例子： panjia@hstc.com<panjia@hstc.com>
    public String getFrom()throws Exception
    {
        InternetAddress address[] = (InternetAddress[])mimeMessage.getFrom();
        String from = address[0].getAddress();
        if(from == null) from="";
        String personal = address[0].getPersonal();
        if(personal == null) personal="";
        String fromaddr = personal+"<"+from+">";
        return fromaddr;
    }

    public String getSubject()throws MessagingException
    {
        String subject = "";
        try
        {
            subject = MimeUtility.decodeText(mimeMessage.getSubject());
            if(subject == null) subject="";
        }
        catch(Exception exce){ }
        return subject;
    }

    public String getMailAddress(String type)throws Exception
    {
        String mailaddr = "";
        String addtype = type.toUpperCase();
        InternetAddress []address = null;
        if(addtype.equals("TO") || addtype.equals("CC") ||addtype.equals("BCC"))
        {
            if(addtype.equals("TO"))
            {
                address = (InternetAddress[])mimeMessage.getRecipients(Message.RecipientType.TO);
            }
            else if(addtype.equals("CC"))
            {
                address = (InternetAddress[])mimeMessage.getRecipients(Message.RecipientType.CC);
            }
            else
            {
                address = (InternetAddress[])mimeMessage.getRecipients(Message.RecipientType.BCC);
            }
            if(address != null)
            {
                for(int i=0;i<address.length;i++)
                {
                    String email=address[i].getAddress();
                    if(email==null) email="";
                    else
                    {
                        email=MimeUtility.decodeText(email);
                    }
                    String personal=address[i].getPersonal();
                    if(personal==null) personal="";
                    else
                    {
                        personal=MimeUtility.decodeText(personal);
                    }
                    String compositeto=personal+"<"+email+">";
                    mailaddr+=","+compositeto;
                }
                mailaddr=mailaddr.substring(1);
            }
        }
        else
        {
            throw new Exception("Error emailaddr type!");
        }
        return mailaddr;
    }

    public String getBodyText()
    {
        return bodytext.toString();
    }

    public void getMailContent(Part part)throws Exception
    {
        String contenttype = part.getContentType();
        int nameindex = contenttype.indexOf("name");
        boolean conname =false;
        if(nameindex != -1) conname=true;
        System.out.println("CONTENTTYPE: "+contenttype);
        if(part.isMimeType("text/plain") && !conname)
        {
            //bodytext.append((String)part.getContent());
        }
        else if(part.isMimeType("text/html") && !conname)
        {
            bodytext.append((String)part.getContent());
        }
        else if(part.isMimeType("multipart/*"))
        {
            Multipart multipart = (Multipart)part.getContent();
            int counts = multipart.getCount();
            for(int i=0;i<counts;i++)
            {
                getMailContent(multipart.getBodyPart(i));
            }
        }
        else if(part.isMimeType("message/rfc822"))
        {
            getMailContent((Part)part.getContent());
        }
        else if(part instanceof MimeBodyPart){
            MimeBodyPart bodyPart = (MimeBodyPart)part;
            if(bodyPart.getContentID() != null){
                String strFileNmae = part.getFileName();
                if (strFileNmae!=null &&!strFileNmae.equals("")) {    // MimeUtility.decodeText解决附件名乱码问题
                    strFileNmae = MimeUtility.decodeText(strFileNmae);
                }else {
                    strFileNmae=bodyPart.getContentID();
                }

                    System.out.println("发现附件: " + strFileNmae);
                    System.out.println("发现附件: " + bodyPart.getContentID());

                    InputStream in = part.getInputStream();// 打开附件的输入流
                    // 读取附件字节并存储到文件中
                    FileOutputStream out = new FileOutputStream(session.getServletContext().getRealPath("/")+Const.IMAGE+strFileNmae);
                    //创建一个缓冲区
                    byte buffer[] = new byte[4096];
                    //判断输入流中的数据是否已经读完的标识
                    int len = 0;
                    //循环将输入流读入到缓冲区当中，(len=in.read(buffer))>0就表示in里面还有数据
                    while((len=in.read(buffer))>0){
                        //使用FileOutputStream输出流将缓冲区的数据写入到指定的目录(savePath + "\\" + filename)当中
                        out.write(buffer, 0, len);
                    }
                    in.close();
                    out.close();
                    map.put(bodyPart.getContentID(),"/webmail_war_exploded"+Const.IMAGE+strFileNmae);

            }else {
                //getMailContent(bodyPart);
            }
        }
        //处理图片
        String string = bodytext.toString();
        Set<Map.Entry<String, String>> entries = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entries.iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> next = iterator.next();
            String key = next.getKey();
            System.out.println(string);
            System.out.println("cid:"+key.substring(1,key.length()-1));
            if(key.indexOf("<")==0) {
                string = string.replaceAll("cid:" + key.substring(1, key.length() - 1), next.getValue());
            }else {
                string = string.replaceAll("cid:" + key, next.getValue());
            }
        }
        this.bodytext=new StringBuffer(string);
        //Marking mail as read status.
//        if(!mimeMessage.isSet(Flags.Flag.SEEN))
//            mimeMessage.setFlag(Flags.Flag.SEEN, true);
    }

    public List<AttachmentDTO> handleMultipart() throws Exception
    {
        List<AttachmentDTO> attachments = new ArrayList<AttachmentDTO>();
        String disposition=null;
        BodyPart part;
        String contenttype = mimeMessage.getContentType();
        int nameindex = contenttype.indexOf("name");
        boolean conname =false;
        if(nameindex != -1) conname=true;

        if(mimeMessage.isMimeType("text/plain") && !conname)
        {
           return  attachments;
        }
        else if(mimeMessage.isMimeType("text/html") && !conname)
        {
            return  attachments;
        }

        Multipart mp = (Multipart) mimeMessage.getContent();
        int mpCount = mp.getCount();
        AttachmentDTO attachmentDTO;
        for (int m = 0; m < mpCount; m++)
        {
            part = mp.getBodyPart(m);
            try {
                disposition = part.getDisposition();
            if (disposition != null && disposition.equals(Part.ATTACHMENT))
            {
                String fileName = null;
                fileName = MimeUtility.decodeText(part.getFileName()); //改了
                attachmentDTO = new AttachmentDTO();
                attachmentDTO.setAttName(fileName);
                attachmentDTO.setMpid(m);
                attachments.add(attachmentDTO);
            }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return attachments;
    }

    /**
     * 　　*　判断此邮件是否需要回执，如果需要回执返回"true",否则返回"false" 　
     */
    public boolean getReplySign()throws MessagingException
    {
        boolean replysign = false;
        String needreply[] = mimeMessage.getHeader("Disposition-Notification-To");
        if(needreply != null)
        {
            replysign = true;
        }
        return replysign;
    }

    public double getSize() throws Exception
    {
        float size = mimeMessage.getSize();
        size = size/1024;
        return size;
    }

    public String getMessageId()throws MessagingException
    {
        return mimeMessage.getMessageID();
    }

    //已读返回true，未读返回false
    public boolean isNew()throws MessagingException
    {
        boolean isnew = false;
        Flags flags = ((Message)mimeMessage).getFlags();
        Flags.Flag []flag = flags.getSystemFlags();
        for(int i=0;i<flag.length;i++)
        {
            if(flag[i] == Flags.Flag.SEEN)
            {
                isnew=true;
                break;
            }
        }
        return isnew;
    }

    public String getSentDate()throws Exception
    {
        Date sentdate = mimeMessage.getSentDate();

        return DateTimeUtil.dateToStr(sentdate);
    }




}
