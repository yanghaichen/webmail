package com.hstc.util;

import com.hstc.common.Const;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import java.util.Properties;

/**
 * 发送邮件的类
 */
public class SMTPHelp {

    static String protocol = "smtp";
    public static Session createSession() {
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", protocol);
        /*必须将mail.smtp.auth属性设置为true，SMTPTransport对象
        才会向SMTP服务器提交用户认证信息
         */
        props.setProperty("mail.debug", "true");
        // 发送服务器需要身份验证
        props.setProperty("mail.smtp.auth", "true");
        // 设置邮件服务器主机名
        props.setProperty("mail.host", Const.SERVER);
        // 发送邮件协议名称
        props.setProperty("mail.transport.protocol", "smtp");
        Session session = Session.getInstance(props);
        session.setDebug(true);
        return session;
    }

    public static String getFrom(String username) {
        return username + "@zjnszn.xyz";
    }

    //返回html的MimeBodyPart
    public static MimeBodyPart createHtmlContent(String body) throws Exception {
        MimeBodyPart contentPart = new MimeBodyPart();
        MimeMultipart contentMultipart = new MimeMultipart("alternative");
        MimeBodyPart htmlBodyPart = new MimeBodyPart();
        htmlBodyPart.setContent(body, "text/html;charset=utf-8");
        //MimeBodyPart textBodyPart = new MimeBodyPart();
        //textBodyPart.setContent(body, "text/plain;charset=utf-8");
        contentMultipart.addBodyPart(htmlBodyPart);
        //contentMultipart.addBodyPart(textBodyPart);
        contentPart.setContent(contentMultipart);
        return contentPart;
    }

    //添加附件，返回带有附件的MimeBodyPart
    public static MimeBodyPart createAttachment(String filename) throws Exception {

        MimeBodyPart attachPart = new MimeBodyPart();
        FileDataSource fds = new FileDataSource(filename);
        attachPart.setDataHandler(new DataHandler(fds));
        attachPart.setFileName(MimeUtility.encodeText(fds.getName()));//如果附件有中文通过转换没有问题了
        return attachPart;
    }

    //返回带图片的MimeBodyPart
    public static MimeBodyPart createPictureContent(String body, String filename) throws Exception {

        MimeBodyPart contentPart = new MimeBodyPart();
        MimeMultipart contentMultipart = new MimeMultipart("related");
        MimeBodyPart htmlBodyPart = new MimeBodyPart();
        htmlBodyPart.setContent(body, "text/html;charset=utf-8");
        contentMultipart.addBodyPart(htmlBodyPart);
        /*
         创建一个表示图片内容的MimeBodyPart对象，
        并将它加入到前面创建的MimeMultipart对象中
         */
        MimeBodyPart gifBodyPart = new MimeBodyPart();
        FileDataSource fds = new FileDataSource(filename);
        gifBodyPart.setDataHandler(new DataHandler(fds));
        gifBodyPart.setContentID("picture");
        contentMultipart.addBodyPart(gifBodyPart);
        //设置完要注意调用saveChaneges方法进行更新
        contentPart.setContent(contentMultipart);
        return contentPart;
    }

    public static Folder getFolder(String host, String username, String password) {
            Properties prop = new Properties();
            prop.setProperty("mail.store.protocol", "smtp");
            prop.setProperty("mail.smtp.host", host);
            Session mailSession = Session.getDefaultInstance(prop, null);
            mailSession.setDebug(false);
            try{
                Store store = mailSession.getStore("pop3");
                store.connect(host, username, password);
                Folder folder = store.getFolder("inbox");
                folder.open(Folder.READ_WRITE);
                return folder;

            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return null;
    }
}
