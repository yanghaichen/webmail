package com.hstc.dto;

import com.hstc.dao.StatusDAO;
import com.hstc.db.DbFactory;
import com.hstc.pojo.Status;
import com.hstc.util.SMTPHelp;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @auther 倪万佳
 * @date 2018/11/7 13:12
 */
public class MessageDTO {

    // 邮件主题
    private String subject;
    // 发件人地址列表
    private String from;
    // 邮件发送时间
    private String sendData;
    // 用户邮件的序号数
    private Integer msgnum;
    // 收件人地址列表
    private String toList;
    // 抄送地址列表
    private String ccList;
    //附件
    private Vector<File> attFile;
    //邮件附件
    private List<AttachmentDTO> attachments;
    //新邮件
    private Boolean isNew;
    //是否被打开
    private Boolean  isSeen;
    private String messageName;
    private String messageContent;
    private Integer id;
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public static void main(String[] args) throws Exception{

    }
    public static List<MessageDTO> getMessageDTOList(String user){
        List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>();
        Connection connection=DbFactory.getConnection();
        String sql = "select message_name,sender,last_updated,s.seen,s.uid, " +
                "substr(substr(message_body,instr(message_body,\"Subject: \")+9),1,(instr(substr(message_body,instr(message_body,\"Subject: \")+9),\"\\n\")-2)) subject " +
                "from inbox t  " +
                "left join status s on t.message_name=s.uid "+
                "where t.repository_name=? order by last_updated";
        ResultSet rs=null;
        PreparedStatement preparedStatement = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user);
            rs = preparedStatement.executeQuery();
            StatusDAO statusDAO = new StatusDAO();
            int i=0;
            while (rs.next()){
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setMessageName(rs.getString(1));
                messageDTO.setFrom(rs.getString(2));
                messageDTO.setSubject(MimeUtility.decodeText(rs.getString(6)));
                String status = rs.getString(5);
                Integer seen = rs.getInt(4);
                if(status != null) {
                    messageDTO.setNew(false);
                    messageDTO.setSeen(seen == 0? false:true);
                }else {
                    messageDTO.setNew(true);
                    messageDTO.setSeen(false);
                    statusDAO.addUid(new Status(user, rs.getString(1)), DbFactory.getConnection());
                }
                messageDTO.setMsgnum(i++);
                messageDTO.setSendData(simpleDateFormat.format(rs.getTimestamp(3)));
                messageDTOList.add(messageDTO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection=DbFactory.getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1,user);
                rs = preparedStatement.executeQuery();
                StatusDAO statusDAO = new StatusDAO();
                int i=0;
                while (rs.next()){
                    MessageDTO messageDTO = new MessageDTO();
                    messageDTO.setMessageName(rs.getString(1));
                    messageDTO.setFrom(rs.getString(2));
                    messageDTO.setSubject(MimeUtility.decodeText(rs.getString(6)));
                    String status = rs.getString(5);
                    Integer seen = rs.getInt(4);
                    if(status != null) {
                        messageDTO.setNew(false);
                        messageDTO.setSeen(seen == 0? false:true);
                    }else {
                        messageDTO.setNew(true);
                        messageDTO.setSeen(false);
                        statusDAO.addUid(new Status(user, rs.getString(1)), DbFactory.getConnection());
                    }
                    messageDTO.setMsgnum(i++);
                    messageDTO.setSendData(simpleDateFormat.format(rs.getTimestamp(3)));
                    messageDTOList.add(messageDTO);
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }catch (Exception e2){
            e2.printStackTrace();
        }finally
        {
            DbFactory.closeAll(rs,preparedStatement, connection);
        }
        Collections.reverse(messageDTOList);
        return messageDTOList;
    }

    public MimeMessage getMimeMessage(String username,Integer msgnum){
        Connection connection = DbFactory.getConnection();
        ResultSet rs=null;
        PreparedStatement preparedStatement = null;
        MimeMessage mimeMessage=null;
        String sql1 = "select message_body,message_name from inbox t where repository_name=? limit ?,1";
        ResultSet rs1 = null;
        try {
            preparedStatement = connection.prepareStatement(sql1);
            preparedStatement.setString(1,username);
            preparedStatement.setInt(2,msgnum);
            rs1 = preparedStatement.executeQuery();
            if (rs1.next()) {
                this.uid=rs1.getString(2);
                byte[] bytes = rs1.getBytes(1);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
                 mimeMessage = new MimeMessage(SMTPHelp.createSession(), byteArrayInputStream);
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            try {
                connection=DbFactory.getConnection();
                preparedStatement = connection.prepareStatement(sql1);
                preparedStatement.setString(1,username);
                preparedStatement.setInt(2,msgnum);
                rs1 = preparedStatement.executeQuery();
                if (rs1.next()) {
                    this.uid=rs1.getString(2);
                    byte[] bytes = rs1.getBytes(1);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
                    mimeMessage = new MimeMessage(SMTPHelp.createSession(), byteArrayInputStream);
                }
            }  catch (Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }finally
        {
            DbFactory.closeAll(rs,preparedStatement, connection);
        }
        return mimeMessage;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSendData() {
        return sendData;
    }

    public void setSendData(String sendData) {
        this.sendData = sendData;
    }

    public Integer getMsgnum() {
        return msgnum;
    }

    public void setMsgnum(Integer msgnum) {
        this.msgnum = msgnum;
    }


    public String getToList() {
        return toList;
    }

    public void setToList(String toList) {
        this.toList = toList;
    }

    public String getCcList() {
        return ccList;
    }

    public void setCcList(String ccList) {
        this.ccList = ccList;
    }

    public Vector<File> getAttFile() {
        return attFile;
    }

    public void setAttFile(Vector<File> attFile) {
        this.attFile = attFile;
    }

    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public Boolean getSeen() {
        return isSeen;
    }

    public void setSeen(Boolean seen) {
        isSeen = seen;
    }

    public void delete(String line) {
        Connection connection = DbFactory.getConnection();
        ResultSet rs=null;
        PreparedStatement preparedStatement = null;
        String sql1 = "update inbox set has_deleted=2  " +
                "where message_name in ("+line+")";
        ResultSet rs1 = null;
        try {
            preparedStatement = connection.prepareStatement(sql1);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            try {
                connection=DbFactory.getConnection();
                preparedStatement = connection.prepareStatement(sql1);
                preparedStatement.executeUpdate();
            }  catch (Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }finally
        {
            DbFactory.closeAll(rs,preparedStatement, connection);
        }
    }
}
