package com.hstc.dto;

import com.hstc.db.DbFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Send {
    private Integer id;
    private String sendName;
    private String recipients;
    private String subject;
    private String messageBody;
    private java.util.Date date;

    public Send() {}

    public Send(Integer id, String sendName, String recipients, String subject, String messageBody, java.util.Date date) {
        this.id = id;
        this.sendName = sendName;
        this.recipients = recipients;
        this.subject = subject;
        this.messageBody = messageBody;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSendName() {
        return sendName;
    }

    public void setSendName(String sendName) {
        this.sendName = sendName;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public java.util.Date getDate() {
        return date;
    }

    public void setDate(java.util.Date date) {
        this.date = date;
    }

    public void addSend(Connection connection) {
        String id="select max(id) from send";
        String sql = "insert into send values(null,?,?,?,?,now())";
        ResultSet rs=null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,this.sendName);
            preparedStatement.setString(2,this.recipients);
            preparedStatement.setString(3,this.subject);
            preparedStatement.setString(4,this.messageBody);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(id);
            rs = preparedStatement.executeQuery();
            if(rs.next()){
                this.id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection=DbFactory.getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1,this.sendName);
                preparedStatement.setString(2,this.recipients);
                preparedStatement.setString(3,this.subject);
                preparedStatement.setString(4,this.messageBody);
                preparedStatement.executeUpdate();
                preparedStatement = connection.prepareStatement(id);
                rs = preparedStatement.executeQuery();
                if(rs.next()){
                    this.id = rs.getInt(1);
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally
        {
            DbFactory.closeAll(rs,preparedStatement, connection);
        }
    }

    public List<Send> getListSend(String username, Connection connection) {
        List<Send> data=new ArrayList<>();
        String sql = "select * from send where send_name=? order by create_time desc";
        PreparedStatement preparedStatement = null;
        ResultSet rs=null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
             rs = preparedStatement.executeQuery();
            while (rs.next()){
                Integer id = rs.getInt(1);
                String sendName = rs.getString(2);
                String recipients = rs.getString(3);
                String subject = rs.getString(4);
                String messageBody = rs.getString(5);
                java.util.Date date = rs.getTimestamp(6);
                Send send = new Send(id,sendName,recipients,subject,messageBody,date);
                data.add(send);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally
        {
            DbFactory.closeAll(rs,preparedStatement, connection);
        }
        return data;
    }
    public Send getSend(Integer id1, Connection connection) {
        String sql = "select * from send where id=?";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id1);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()){
                Integer id = rs.getInt(1);
                String sendName = rs.getString(2);
                String recipients = rs.getString(3);
                String subject = rs.getString(4);
                String messageBody = rs.getString(5);
                java.util.Date date = rs.getDate(6);
                Send send = new Send(id,sendName,recipients,subject,messageBody,date);
                return send;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally
        {
            DbFactory.closeAll(null,preparedStatement, connection);
        }
        return null;
    }

    public void deleted(Integer[] checkedIdArray2, Connection connection) {
        String ids="";
        for(Integer integer:checkedIdArray2){
            ids+=","+integer;
        }
        String sql = "delete from send where id in ("+ids.substring(1)+")";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            Integer rs = preparedStatement.executeUpdate();
            sql="delete from files where send_id in ("+ids.substring(1)+")";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally
        {
            DbFactory.closeAll(null,preparedStatement, connection);
        }
    }
}
