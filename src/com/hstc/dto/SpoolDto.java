package com.hstc.dto;

import com.hstc.db.DbFactory;
import com.hstc.pojo.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SpoolDto {
    private String messageName;
    private String repositoryName;
    private String messageState;
    private String errorMessage;
    private String sender;
    private String recipients;
    private String remoteHost;
    private String remoteAddr;
    private byte[] messageBody;
    private byte[] messageAttributes;
    private Date lastUpdated;
    private String userName;

    public SpoolDto() { }

    public SpoolDto(String messageName, String repositoryName, String messageState, String errorMessage, String sender, String recipients, String remoteHost, String remoteAddr, byte[] messageBody, byte[] messageAttributes, Date lastUpdated) {
        this.messageName = messageName;
        this.repositoryName = repositoryName;
        this.messageState = messageState;
        this.errorMessage = errorMessage;
        this.sender = sender;
        this.recipients = recipients;
        this.remoteHost = remoteHost;
        this.remoteAddr = remoteAddr;
        this.messageBody = messageBody;
        this.messageAttributes = messageAttributes;
        this.lastUpdated = lastUpdated;
    }

    //通过username查找uid
    public List<SpoolDto> getSpoolDtos(SpoolDto spoolDto, Connection conn)
    {
        List<SpoolDto> data=new ArrayList<>();
        String sql = "select * from spool where sender like ?" ;
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1,spoolDto.getUserName()+"@");
            rs = preparedStatement.executeQuery();
            while (rs.next()){
                String messageName = rs.getString(1);
                String repositoryName = rs.getString(2);
                String messageState = rs.getString(3);
                String errorMessage = rs.getString(4);
                String sender = rs.getString(5);
                String recipients = rs.getString(6);
                String remoteHost = rs.getString(7);
                String remoteAddr = rs.getString(8);
                byte[] messageBody = rs.getBytes(9);
                byte[] messageAttributes = rs.getBytes(10);
                Date lastUpdated = rs.getDate(11);
                SpoolDto spoolDto1 = new SpoolDto(messageName,repositoryName,messageState,errorMessage,sender,recipients
                ,remoteHost,remoteAddr,messageBody,messageAttributes,lastUpdated);
                data.add(spoolDto1);
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            DbFactory.closeAll(rs,preparedStatement, conn);
        }
        return data;
    }

    public String getMessageState() {
        return messageState;
    }

    public void setMessageState(String messageState) {
        this.messageState = messageState;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public byte[] getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(byte[] messageBody) {
        this.messageBody = messageBody;
    }

    public byte[] getMessageAttributes() {
        return messageAttributes;
    }

    public void setMessageAttributes(byte[] messageAttributes) {
        this.messageAttributes = messageAttributes;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

}
