<%@ page import="com.hstc.common.ServerResponse" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>邮箱管理</title>
    <link rel="stylesheet" href="<%=basePath%>assets/css/amazeui.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<%=basePath%>assets/css/core.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/menu.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/index.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/admin.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/delete.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/typography.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/form.css" />
</head>
<body>
<!-- Begin page -->
<header class="am-topbar am-topbar-fixed-top">
    <div class="am-topbar-left am-hide-sm-only">
        <a href="#" class="logo">
            <c:choose>
                <c:when test="${not empty sessionScope.currentUser}"><span><c:out  value="${sessionScope.currentUser.username}" /></c:when>
                <c:otherwise>
                未登陆
                </c:otherwise>
            </c:choose></span><i class="zmdi zmdi-layers"></i></a>&nbsp;&nbsp;<a  href="<%=basePath%>user/logout">退出</a>
            <p style="margin: 0px;padding: 0px">邮箱 </p>
    </div>
    <input type="hidden" class="msg" value="${msg}"/>
    <div class="contain">
        <ul class="am-nav am-navbar-nav am-navbar-left">
            <li><h4 class="page-title">已删除</h4></li>
        </ul>

        <ul class="am-nav am-navbar-nav am-navbar-right">
            <li class="inform"><i class="am-icon-bell-o" aria-hidden="true"></i></li>
            <li class="hidden-xs am-hide-sm-only">
                <form role="search" class="app-search">
                    <input type="text" name="searchKey" placeholder="邮件查询..." class="form-control">
                    <a href="javascript:void(0);" class="search_btn"><img src="../assets/img/search.png"></a>
                </form>
            </li>
        </ul>
    </div>
</header>
<!-- end page -->


<div class="admin">

    <!-- ========== Left Sidebar Start ========== -->
    <!-- sidebar start -->
    <div class="admin-sidebar am-offcanvas  am-padding-0" id="admin-offcanvas">
        <div class="am-offcanvas-bar admin-offcanvas-bar">
            <!-- User -->
            <div class="user-box am-hide-sm-only">
                <div class="user-img">
                    <img src="../assets/img/avatar-1.jpg" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status offline"><i class="am-icon-dot-circle-o" aria-hidden="true"></i></div>
                </div>
                <h5><a href="#">Mat Helme</a> </h5>
                <ul class="list-inline">
                    <li>
                        <a href="#">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="text-custom">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="admin-parent">
                        <hr/>

                    </li>
                </ul>
            </div>
            <!-- End User -->

            <ul class="am-list admin-sidebar-list">
                <li><a href="<%=basePath%>jsp/index.jsp"><span class="am-icon-home"></span> 首页</a></li>
                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav1'}"><span class="am-icon-table"></span> 邮件 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                    <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav1">
                        <li><a href="<%=basePath%>jsp/index.jsp" class="am-cf"> <span>收件箱</span></a></li>
                        <li><a href="<%=basePath%>jsp/sendEmail.jsp">写邮件</a></li>
                        <li><a href="<%=basePath%>jsp/index1.jsp">已发送</a></li>
                        <li><a href="<%=basePath%>jsp/draft.jsp">草稿箱</a></li>
                        <li><a href="<%=basePath%>jsp/delete.jsp">已删除</a></li>
                    </ul>
                </li>
                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav2'}"><span class="am-icon-file"></span> 我的好友 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                    <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav2">
                        <c:choose>
                        <c:when test="${not empty sessionScope.userNameList}">
                            <c:forEach var="user" items="${sessionScope.userNameList}">
                                <li><a href="javascript:void(0);" class="am-cf  communion"> <span>${user.username}</span></a></li>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                        </c:otherwise>
                        </c:choose>
                    </ul>
                </li>

                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav3'}">  </a>
                    <ul class="am-list am-collapse admin-sidebar-sub " id="collapse-nav3">
                        <li><a href="javascript:void(0);" class="am-cf"> <span></span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- sidebar end -->


    <!-- ========== Left Sidebar end ========== -->




    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="card-box">
                <!-- Row start -->
                <div class="am-g">
                    <div class="am-u-sm-12 am-u-md-6">
                        <div class="am-btn-toolbar">
                            <div class="am-btn-group am-btn-group-xs">
                                <button type="button" class="am-btn am-btn-default flush_btn"><span class="am-icon-plus"></span> 刷新</button>
                                <button type="button" class="am-btn am-btn-default del_btn"><span class="am-icon-trash-o"></span> 删除</button>
                            </div>
                        </div>
                    </div>


                    <div class="am-u-sm-12 am-u-md-4">

                  <%--<span>--%>
                   <%--&nbsp;&nbsp;&nbsp;共0封邮件，0封未读，0封已读--%>
                  <%--</span>--%>

                    </div>
                </div>

                <!-- Row end -->

                <!-- Row start -->
                <div class="am-g">
                    <div class="am-u-sm-12">
                        <form class="am-form">
                            <table class="am-table am-table-striped am-table-hover table-main">
                                <thead>
                                <tr>
                                    <th class="table-check"><input type="checkbox" class="parent_check" /></th><th class="table-id">序号</th><th class="table-title">标题</th><th class="table-type">回复</th><th class="table-author am-hide-sm-only">转发</th><th class="table-type">发送人</th><th class="table-date am-hide-sm-only">发送时间</th><th class="table-type">新旧邮件</th><th class="table-type">读否</th>
                                </tr>
                                </thead>
                                <tbody  class="mailcontent dataBox">
                                        <%--显示分页数据--%>
                                </tbody>
                            </table>
                            <div class="am-cf mypage">
                                    <%--显示分页条--%>
                            </div>
                            <hr />

                        </form>
                    </div>

                </div>
                <!-- Row end -->

            </div>




        </div>


    </div>
</div>
<!-- end right Content here -->
<!--</div>-->
</div>
</div>
</body>
<!-- navbar -->
<a href="admin-offcanvas" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"><!--<i class="fa fa-bars" aria-hidden="true"></i>--></a>

<script type="text/javascript" src="<%=basePath%>assets/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=basePath%>assets/js/amazeui.min.js"></script>
<%--<script type="text/javascript" src="<%=basePath%>assets/js/app.js" ></script>--%>

<script>

    var size = 10;      //每页的数据行
    var total = 0;     //总数据量
    var startPage = 0;  //开始页号
    var endPage = 0;    //尾页号
    var hitPage = 0;    //激活页号
    var pages = 0;    //页数
    var showpages = 5; //分页条展示多少页数
    var flag = 1;      //-1表示向上翻页，1表示向下翻页
    var data;
    var isNewCount = 0; //新邮件总数
    $(function() {
        var msg = $('.msg').val();
        if (msg) {
            alert(msg);
        }
        getMailList();

        //全局的checkbox选中和未选中的样式
        $parentChexbox = $('.parent_check'), //全选
            $dataBox = $('.dataBox'),            //用于判断全局与子类的关系
            $sonCheckBox = $('.son_check');      //单个子类选中

        //全局全选与单个的关系
        $parentChexbox.click(function () {
            var $checkboxs = $dataBox.find('input[type="checkbox"]');
            if ($(this).is(':checked')) {
                $checkboxs.prop("checked", true);
            } else {
                $checkboxs.prop("checked", false);
            }
        });
        $(document).on('click', '.son_check', function (e) {
            //判断：所有单个是否勾选
            var $checkboxs = $dataBox.find('input[type="checkbox"]');
            var len = $checkboxs.length;
            var num = 0;
            $checkboxs.each(function () {
                if ($(this).is(':checked')) {
                    num++;
                }
            });
            if (num == len) {
                $parentChexbox.prop("checked", true);
            } else {
                //单个取消勾选，全局全选取消勾选
                $parentChexbox.prop("checked", false);
            }
        });
    });

    //获取邮箱列表
    function getMailList() {
        $.ajax({
            url:"<%=basePath%>mail/maillist",
            type: "get",
            contentType:"application/json",
            success: function (result, status) {
                data = JSON.parse(result).messageDTOList;
                isNewCount = JSON.parse(result).isNewCount;
                total = data.length;
                pages = total / size ;
                if(total % size != 0) {
                    pages += 1;
                }
                flag = 1;
                setContent(startPage, data);
                pageInfo(startPage, endPage, flag);
            },
            error: function (res) {
                alert("获取数据失败");
            }
        });
    }

    //渲染页面
    function setContent(startPage, data){
        var str;
        if(startPage == 0) {
            startPage = 1;
        }

        var num = (startPage-1) * size ;
        var end = startPage * size;
        if(end > total) {
            end = total;
        }
        var isNew;
        var seen;
        for(var i = num; i < end; i++) {

            if(data[i].new == true) {
                isNew = '新邮件';
            }else if(data[i].new == false) {
                isNew = '旧邮件';
            }
            if(data[i].seen == false) {
                seen = '未读';
            }else {
                seen = '已读';
            }
            str += '<tr>';
            str += '<td><input type="checkbox" class="son_check" del_id="'+ data[i].msgnum + '"/></td>';
            str += '<td>' +  (parseInt(data[i].msgnum)+1)  +'</td>';
            str += '<td><a href="<%=basePath%>jsp/email_content.jsp?msgnum='+  data[i].msgnum + '">' + data[i].subject + '</a></td>';
            str += '<td><a href="<%=basePath%>jsp/reply.jsp?msgnum=' + data[i].msgnum + '">回复</a></td>';
            str += '<td><a href="<%=basePath%>jsp/forward.jsp?msgnum='+ data[i].msgnum +'">转发</a></td>';
            str += '<td class="am-hide-sm-only">' + data[i].from +'</td>';
            str += '<td class="am-hide-sm-only">'+ data[i].sendData  + '</td>';
            str += '<td>'+ isNew + '</td>';
            str += '<td>'+ seen + '</td>';
            str += '</tr>';
        }
        if(data.length>0 && (str != "" || str != null)){
            $(".mailcontent").html(str);
        }else{
            $(".mailcontent").html("<br/><span style='width:50%;height:100px;display:block;margin:0 auto;'>暂无数据</span>");

        }
    }


    //渲染点击两分页图标箭头的功能
    function pageInfo(startPage, endPage, flag){
        //如果startPage == 1的话，'«'图标不显示， 如果endPage >= pages, '»'图标不显示,flag判断是那个图标被点击了
        var str2 = '';

        str2 += '共'+ total + '条记录';
        str2 += '<div class="am-fr">';
        str2 += '<ul class="am-pagination">';

        if(flag == 1){
            if(startPage == 0) {     //初始化时
                startPage = 1;
            }else{
                startPage = startPage + showpages;
            }
            if(hitPage <= 0) {
                hitPage = 1;
            }else {
                hitPage += showpages;
                if(hitPage > pages){
                    hitPage = pages;
                }
            }
            endPage = endPage + showpages;
        }else if(flag == -1) {
            startPage = startPage - showpages;
            if(startPage <= 0) {
                startPage = 1;
            }
            if(hitPage <= 0) {
                hitPage = 1;
            }else {
                hitPage -= showpages;
                if(hitPage <= 0){
                    hitPage = 1;
                }
            }
            endPage = startPage + showpages - 1;
        }


        if(endPage > pages){
            endPage = pages;
        }

        if(startPage != 1) {
            str2 += '<li class="am-disabled front"><a href="#">«</a></li>';
        }




        for(var i = startPage ; i <= endPage; i++ ) {
            if( i == hitPage){
                if(i == startPage){
                    str2 += '<li class="am-active startPage hitPage"><a href="javascript:void(0);">'+ i + '</a></li>';
                }else if(i == endPage){
                    str2 += '<li class="am-active endPage hitPage"><a href="javascript:void(0);">'+ i + '</a></li>';
                }else {
                    str2 += ' <li class="am-active hitPage"><a href="javascript:void(0);">'+ i + '</a></li>';
                }
            }else{
                if(i == startPage){
                    str2 += '<li class="startPage hitPage"><a href="javascript:void(0);">'+ i + '</a></li>';
                }else if(i == endPage){
                    str2 += '<li class="endPage hitPage"><a href="javascript:void(0);">'+ i + '</a></li>';
                }else {
                    str2 += ' <li class="hitPage" ><a href="javascript:void(0);">'+ i + '</a></li>';
                }
            }

        }
        if(endPage < pages){
            str2 += '<li class="am-disabled up"><a href="#">»</a></li>';
        }
        str2 += '</ul>';
        str2 += '</div>';
        $(".mypage").html(str2);
    }

    //更新相应页面的渲染
    $(document).on('click', '.hitPage', function(e) {

        hitPage = $(this).children().text();
        setContent(hitPage, data);
        $(this).addClass('am-active');
        $(this).siblings().removeClass('am-active');
    });

    $(document).on('click', '.front', function(e) {
        startPage = parseInt($('.startPage').children().text());
        endPage = parseInt($('.endPage').children().text());
        flag = -1;
        pageInfo(startPage, endPage, flag);
        setContent(hitPage, data);
    });
    $(document).on('click', '.up', function(e) {
        startPage = parseInt($('.startPage').children().text());
        endPage = parseInt($('.endPage').children().text());
        flag = 1;
        pageInfo(startPage, endPage, flag);
        setContent(hitPage, data);

    });

    //删除邮件
    $('.del_btn').click(function(){
        var $checkboxs = $dataBox.find('input[type="checkbox"]');
        var checkedIdArray=new Array();
        $checkboxs.each(function () {
            if ($(this).is(':checked')) {
                checkedIdArray.push($(this).attr('del_id'));

            }
        });
        if(checkedIdArray.length == 0){
            alert("请勾选需要删除的邮件");
        }
        $.ajax({
            url: "<%=basePath%>mail/killmail",
            type: "GET",
            data:{'checkedIdArray':checkedIdArray.toString()},
            success: function (data) {
                alert(data);
                window.location.href = "<%=basePath%>jsp/index.jsp";
            },
            error: function (returndata) {

            }
        });
    });

    //刷新数据
    $('.flush_btn').click(function() {
        window.location.href = "<%=basePath%>jsp/index.jsp";
    });

    //查询邮件
    $('.search_btn').click(function() {
        var searchKey = $("input[name='searchKey']").val();
        $.ajax({
            url:"<%=basePath%>mail/searchmail",
            type: "GET",
            data: {'searchKey':searchKey},
            contentType:"application/json",
            success: function (result, status) {
                data = JSON.parse(result).messageDTOList;
                total = data.length;
                pages = total / size ;

                if(total % size != 0) {
                    pages += 1;
                }
                flag = 1;
               startPage = 0;  //开始页号
               endPage = 0;    //尾页号
               hitPage = 0;    //激活页号
                setContent(startPage, data);
                pageInfo(startPage, endPage, flag);
            },
            error: function (res) {
                alert("获取数据失败");
            }
        });

    });

    //给好友发邮件
    $('.communion').click(function(){
        var username = $(this).text();
        username = username+'@zjnszn.xyz<'+ username +'@zjnszn.xyz>';
        window.location.href = "<%=basePath%>jsp/sendEmail.jsp?toList=" + username;
    });
</script>

</html>
