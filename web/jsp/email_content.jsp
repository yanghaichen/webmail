<%@ page import="com.hstc.pojo.Users" %>
<%@ page import="com.hstc.common.Const" %>
<%@ page import="com.hstc.util.POP3Help" %>
<%@ page import="javax.mail.internet.InternetAddress" %>
<%@ page import="javax.mail.*" %>
<%@ page import="com.hstc.util.DateTimeUtil" %>
<%@ page import="com.hstc.util.PraseMimeMsg" %>
<%@ page import="javax.mail.internet.MimeMultipart" %>
<%@ page import="javax.mail.internet.MimeMessage" %>
<%@ page import="javax.mail.Part" %>
<%@ page import="com.hstc.dto.AttachmentDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sun.mail.pop3.POP3Folder" %>
<%@ page import="com.hstc.pojo.Status" %>
<%@ page import="com.hstc.dao.StatusDAO" %>
<%@ page import="com.hstc.db.DbFactory" %>
<%@ page import="com.hstc.dto.MessageDTO" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>邮件正文</title>
    <link rel="stylesheet" href="<%=basePath%>assets/css/amazeui.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<%=basePath%>assets/css/core.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/menu.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/index.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/admin.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/typography.css" />
    <link rel="stylesheet" href="<%=basePath%>assets/css/page/form.css" />
</head>
<body>
<%
    Users user = (Users) request.getSession().getAttribute(Const.USERNAME);
    // 2、读取邮件夹
    POP3Folder folder = (POP3Folder)request.getSession().getAttribute("folder");
    // 获取邮件夹中第i封邮件信息
    Integer msgnum = Integer.parseInt(request.getParameter("msgnum"));
    String subject = null;
    String from = null;
    String to = null;
    Object content = null;
    String sendDate = null;

    List<AttachmentDTO> attachmentDTOList = null;
    try {
        /*if(!folder.isOpen()){
            folder.open(Folder.READ_WRITE);
        }
        Message[] messages = folder.getMessages();
        MimeMessage message = (MimeMessage) messages[msgnum];*/
        MessageDTO messageDTO = new MessageDTO();
        MimeMessage message = messageDTO.getMimeMessage(user.getUsername(),msgnum);
        PraseMimeMsg pmm = new PraseMimeMsg(message,session);
        subject = pmm.getSubject();
        from = pmm.getFrom();
        to = pmm.getMailAddress("to");
        sendDate = pmm.getSentDate();
        //String uid = folder.getUID(message);
        String uid = messageDTO.getUid();
        Status status = new Status(user.getUsername(),uid);
        status.setSeen(1);
        StatusDAO statusDAO = new StatusDAO();
        statusDAO.updateSeen( status, DbFactory.getConnection());
        pmm.getMailContent((Part)message);
        content = pmm.getBodyText();
        attachmentDTOList = pmm.handleMultipart();



    } catch (MessagingException e) {
        e.printStackTrace();
    }
%>
<!-- Begin page -->
<header class="am-topbar am-topbar-fixed-top">
    <div class="am-topbar-left am-hide-sm-only">
        <a href="#" class="logo">
            <c:choose>
            <c:when test="${not empty sessionScope.currentUser}"><span><c:out  value="${sessionScope.currentUser.username}" /></c:when>
                <c:otherwise>
                    未登陆
                </c:otherwise>
            </c:choose></span><i class="zmdi zmdi-layers"></i></a>&nbsp;&nbsp;<%--<a  href="<%=basePath%>user/logout">退出</a>--%>
    </div>
    <input type="hidden" class="msg" value="${msg}"/>
    <div class="contain">
        <ul class="am-nav am-navbar-nav am-navbar-left">

            <li><h4 class="page-title">邮件内容</h4></li>
        </ul>

    </div>
</header>
<!-- end page -->


<div class="admin">
    <!--<div class="am-g">-->
    <!-- ========== Left Sidebar Start ========== -->
    <!--<div class="left side-menu am-hide-sm-only am-u-md-1 am-padding-0">
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 548px;">
            <div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 548px;">-->
    <!-- sidebar start -->
    <div class="admin-sidebar am-offcanvas  am-padding-0" id="admin-offcanvas">
        <div class="am-offcanvas-bar admin-offcanvas-bar">
            <!-- User -->
            <div class="user-box am-hide-sm-only">
                <div class="user-img">
                    <img src="../assets/img/avatar-1.jpg" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status offline"><i class="am-icon-dot-circle-o" aria-hidden="true"></i></div>
                </div>
                <h5><a href="#">Mat Helme</a> </h5>
                <ul class="list-inline">
                    <li>
                        <a href="#">
                            <i class="am-icon-cog" aria-hidden="true"></i>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="text-custom">
                            <i class="am-icon-cog" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- End User -->


            <ul class="am-list admin-sidebar-list">
                <li><a href="<%=basePath%>jsp/index.jsp"><span class="am-icon-home"></span> 首页</a></li>
                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav1'}"><span class="am-icon-table"></span> 邮件 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                    <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav1">
                        <li><a href="<%=basePath%>jsp/index.jsp" class="am-cf"> <span>收件箱</span></a></li>
                        <li><a href="<%=basePath%>jsp/sendEmail.jsp">写邮件</a></li>
                        <li><a href="<%=basePath%>jsp/index1.jsp">已发送</a></li>
                        <li><a href="<%=basePath%>jsp/draft.jsp">草稿箱</a></li>
                        <li><a href="<%=basePath%>jsp/delete.jsp">已删除</a></li>
                    </ul>
                </li>
                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav2'}"><span class="am-icon-file"></span> 我的好友 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                    <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav2">
                        <c:choose>
                            <c:when test="${not empty sessionScope.userNameList}">
                                <c:forEach var="user" items="${sessionScope.userNameList}">
                                    <li><a href="javascript:void(0);" class="am-cf  communion"> <span>${user.username}</span></a></li>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </li>

                <li class="admin-parent">
                    <a class="am-cf" data-am-collapse="{target: '#collapse-nav3'}">  </a>
                    <ul class="am-list am-collapse admin-sidebar-sub " id="collapse-nav3">
                        <li><a href="javascript:void(0);" class="am-cf"> <span></span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- sidebar end -->

    <!--</div>
</div>
</div>-->
    <!-- ========== Left Sidebar end ========== -->



    <!--	<div class="am-g">-->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="am-g">
                <!-- Row start -->
                <div class="am-u-sm-12">
                    <div class="card-box">

                        <form action="" class="am-form" data-am-validator>
                            <center><h1><%=subject%></h1></center>
                            <div id="content_box"><%=content%></div>
<%--                            正文： <textarea name="content" style="width: 850px;height: 400px;max-width: 850px;max-height: 400px;"></textarea> <br/>--%>
                            <%
                                for(int i  = 0; i < attachmentDTOList.size(); i++) {
                            %>
                            附件：<a href="<%=basePath%>HandleAttachServlet?msgnum=<%=msgnum%>&&bodynum=<%=((AttachmentDTO)attachmentDTOList.get(i)).getMpid()%>&&filename=<%=((AttachmentDTO)attachmentDTOList.get(i)).getAttName()%>"> <%=((AttachmentDTO)attachmentDTOList.get(i)).getAttName()%></a>
                            <%
                               }
                           %>

                            <div><span >发件人:<%=from%></span></div>
                            <div><span>发送时间：<%=sendDate%></span></div>
                            <hr/>
                            <a class="am-btn am-btn-secondary" href="<%=basePath%>jsp/reply.jsp?msgnum=<%=msgnum%>">回复</a>
                            <a class="am-btn am-btn-secondary" href="<%=basePath%>jsp/forward.jsp?msgnum=<%=msgnum%>">转发</a>
                            <a class="am-btn am-btn-secondary" href="<%=basePath%>jsp/index.jsp">返回列表</a>

                        </form>


                    </div>
                </div>
                <!-- Row end -->
            </div>




        </div>
    </div>
    <!-- end right Content here -->
    <!--</div>-->

</div>
</div>

<!-- navbar -->
<a href="admin-offcanvas" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"><!--<i class="fa fa-bars" aria-hidden="true"></i>--></a>


<script type="text/javascript" src="<%=basePath%>assets/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=basePath%>assets/js/amazeui.min.js"></script>
<%--<script type="text/javascript" src="<%=basePath%>assets/js/app.js" ></script>--%>
<script type="text/javascript" src="<%=basePath%>assets/js/blockUI.js" ></script>
</body>
<script>

    $(function(){
        //使textarea的内容以html格式显示
        var $textarea = $("textarea[name='content']");
        $textarea.val($('.Mycontent').text());
        $("textarea[name='content']").attr("readonly", true);
    });
    //给好友发邮件
    $('.communion').click(function(){
        var username = $(this).text();
        username = username+'@zjnszn.xyz<'+ username +'@zjnszn.xyz>';
        window.location.href = "<%=basePath%>jsp/sendEmail.jsp?toList=" + username;
    });
</script>
</html>
